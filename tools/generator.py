import sys
from importlib import metadata
from pathlib import Path

import boto3.session
from boto3.docs import generate_docs


def generate_boto3_docs(path) -> None:
    """
    Generate docs natively from boto3, which generates docs from
    the AWS API to be formatted for use with the boto3 library.

    Args:
        path (pathlib.PosixPath): Root path where the docs should be saved to: `<path>/reference/services`
    """
    print(f"Saving raw boto3 rst to {path}")
    print(f"This will take some time...")
    session = boto3.session.Session(region_name="us-east-1")
    generate_docs(path, session)
    return None


def get_package_versions(packages: list) -> dict:
    """
    Retrieve package versions for a list of Python packages.

    Args:
        packages (list): List of Python packages to query package versions

    Returns:
        dict: Dictionary of packages with their versions

    Example:
        .. code-block:: python

        # Retrieve the versions for boto3 and botocore
        packages = ['boto3','botocore']
        package_versions = get_package_versions(packages)

    .. _Google Python Style Guide:
        http://google.github.io/styleguide/pyguide.html
    """
    package_versions = {package: metadata.version(package) for package in packages}
    return package_versions


def read_boto3_docs():
    return True


def create_idem_awsauto_docstring():
    return True


def create_idem_awsauto_function_code(
    aws_class_name: str, function_with_args: str, function_docstring: list
):
    # Function prep
    tab = "    "  # Four spaces
    aws_class_name_split = aws_class_name.split(".")
    function_name, function_args = function_with_args.split("(")
    function_args = function_args.split(")")[0].split(",")

    # Clients use different call formatting
    if aws_class_name_split[-1] == "Client":
        function_code = [
            f"{tab}return await hub.tool.aws.client.request(\n",
            f"{tab*2}ctx,\n",
        ]
        # Client-formatted args
        function_code.append(f'{tab*2}client="{aws_class_name_split[-2].lower()}",\n')
        function_code.append(f'{tab*2}func="{function_name}",\n')
    else:
        function_code = [
            f"{tab}return await hub.tool.aws.resource.request(\n",
            f"{tab*2}ctx,\n",
        ]
        function_code.append(f'{tab*2}"{aws_class_name_split[-2].lower()}",\n')
        function_code.append(f'{tab*2}"{aws_class_name_split[-1].split("(")[0]}",\n')
        function_code.append(f'{tab*2}"{function_name}",\n')

    # Search for idem-aws special arg substrings
    subs = ["DryRun"]
    subs_found = []
    for sub in subs:
        found = [
            docstring_line
            for docstring_line in function_docstring
            if sub in docstring_line
        ]
        if found:
            subs_found.append(sub)
    # Translate discovered special args into appropriate args
    for sub in subs_found:
        if sub == "DryRun":
            function_code.append(f'{tab*2}dry_run=ctx.get("test", False),\n')

    # Remaining args
    for function_arg in function_args:
        if function_arg == "**kwargs" or "=" in function_arg:
            function_code.append(f"{tab*2}{function_arg},\n")
        else:
            function_code.append(f"{tab*2}{function_arg}={function_arg},\n")
    # Close function
    function_code.append(f"{tab})\n")

    return function_code


def write_idem_awsauto_py():
    return True


def write_idem_awsauto_rst_automod(aws_class_name, idem_code_subpath, idem_rst_path):
    """

    - Create sub folders in target doc dir
    - Create index.rst files
    - Create <classname>/<classname>.rst files
      - Within these files, have automodule call
    """
    automodule_path = str(idem_code_subpath).replace("/", ".")
    aws_function_name = aws_class_name.split(".")[-1].lower()
    rst_file_name = aws_function_name.split("(")[0]
    rst_file_dir = Path(
        idem_rst_path,
        Path(
            aws_service_class.lower()
            .replace(aws_function_name, "")
            .replace(".", "/")
            .lower()
        ),
    )
    rst_file_dir.mkdir(parents=True, exist_ok=True)

    # automodule contents
    rst_file_contents = []
    rst_title = aws_class_name.lower().split("(")[0]
    rst_headerline = len(rst_title) * "="
    rst_file_contents.append(f"{rst_headerline}\n")
    rst_file_contents.append(f"{rst_title}\n")
    rst_file_contents.append(f"{rst_headerline}\n\n")
    rst_file_contents.append(f".. automodule:: {automodule_path}.{rst_title}\n")
    rst_file_contents.append(f"    :members:")

    # Write to rst file
    service_rst_file_idem = Path(rst_file_dir, f"{rst_file_name}.rst")
    with open(service_rst_file_idem, "w") as rst_writer:
        rst_writer.writelines(rst_file_contents)

    return None


def write_idem_awsauto_rst_index(aws_class_name, idem_rst_path):
    rst_parent_dir = "/".join(aws_class_name.lower().split(".")[:-1])
    rst_file_contents = []
    rst_title = rst_parent_dir.replace("/", ".")
    rst_headerline = len(rst_title) * "="
    rst_file_contents.append(f"{rst_headerline}\n")
    rst_file_contents.append(f"{rst_title}\n")
    rst_file_contents.append(f"{rst_headerline}\n\n")
    rst_file_contents.append(".. toctree::\n")
    rst_file_contents.append("  :maxdepth: 2\n")
    rst_file_contents.append("  :glob:\n\n")
    rst_file_contents.append("  *\n")

    # Write to rst file
    service_rst_file_idem = Path(idem_rst_path, rst_parent_dir, f"index.rst")
    with open(service_rst_file_idem, "w") as rst_writer:
        rst_writer.writelines(rst_file_contents)

    return None


if __name__ == "__main__":
    # Generate boto3 rst docs in ../docs/reference
    aws_dep_versions = ["aioboto3", "aiobotocore", "boto3", "botocore"]
    print("Major dependency versions:", get_package_versions(aws_dep_versions))

    idem_doc_path = Path(Path(__file__).absolute().parent.parent, "docs")
    argcount = len(sys.argv)
    service_targets = None
    if argcount > 1:
        if sys.argv[1] == "true":
            generate_boto3_docs(idem_doc_path)
        if argcount > 2:
            service_targets = set(sys.argv[2:])
    else:
        generate_boto3_docs(idem_doc_path)

    # Prepare for conversion to idem-aws-auto code
    idem_function_base_args = "hub, ctx, "
    aws_rst_source_path = Path(idem_doc_path, "reference", "services")
    aws_rst_source_files = [
        filename for filename in aws_rst_source_path.glob("*") if filename.is_file()
    ]
    idem_rst_path = Path(idem_doc_path, "ref", "exec")

    # Create directory idem-aws-auto exec directory structure
    idem_code_subpath = Path("idem_aws_auto", "exec", "awsauto")
    idem_code_path = Path(Path(__file__).absolute().parent.parent, idem_code_subpath)
    idem_code_path.mkdir(parents=True, exist_ok=True)

    # Conversion process of AWS source rst for .py files
    for aws_rst_source_file in aws_rst_source_files:
        aws_service = aws_rst_source_file.name.replace(".rst", "")

        # If args provided for targeted services, skip gen of .py files
        #  for unprovided services
        if service_targets and aws_service not in service_targets:
            continue

        with Path.open(aws_rst_source_file, mode="r") as aws_rst_source_file_reader:
            aws_rst_source = aws_rst_source_file_reader.read().split("\n")

        # Creation of dictionaries for AWS classes and their methods
        python_calls = {}
        index = 0
        for text in aws_rst_source:
            if ".. py:" in text:
                if ".. py:class::" in text:
                    current_class = text.strip().replace(".. py:class:: ", "")
                    python_calls[current_class] = {"index": index, "methods": []}
                elif ".. py:method::" in text:
                    python_calls[current_class]["methods"].append(
                        {"index": index, "python_call": text.strip()}
                    )
            index += 1

        # Create exec directories and associated .py files of functions
        # Makes directory per AWS service, and subdir per subclass
        aws_service_classes = python_calls.keys()
        class_count = len(aws_service_classes)
        class_index = 0
        class_last_index = class_count - 1
        for aws_service_class in aws_service_classes:
            py_file_name = aws_service_class.split(".")[-1].lower()
            py_file_dir = Path(
                idem_code_path,
                Path(
                    aws_service_class.lower()
                    .replace(py_file_name, "")
                    .replace(".", "/")
                    .lower()
                ),
            )
            py_file_dir.mkdir(parents=True, exist_ok=True)

            # Make rst dir structure
            write_idem_awsauto_rst_automod(
                aws_service_class, idem_code_subpath, idem_rst_path
            )
            write_idem_awsauto_rst_index(aws_service_class, idem_rst_path)
            idem_exec_file_contents = []
            method_index = 0
            method_last_index = len(python_calls[aws_service_class]["methods"]) - 1

            # Create function for each method, include docstrings
            for aws_service_class_method in python_calls[aws_service_class]["methods"]:
                idem_function_name, idem_function_args = (
                    aws_service_class_method["python_call"]
                    .replace(".. py:method:: ", "")
                    .split("(")
                )
                idem_function_args = f"{idem_function_base_args}{idem_function_args}"
                idem_exec_file_contents.append("\n\n")
                idem_exec_file_contents.append(
                    f'async def {idem_function_name}({idem_function_args}:\n    """'
                )
                start_read = aws_service_class_method["index"] + 1
                if (method_index + 1) <= method_last_index:
                    end_read = (
                        python_calls[aws_service_class]["methods"][method_index + 1][
                            "index"
                        ]
                        - 1
                    )
                    method_index += 1
                elif (class_index + 1) <= class_last_index:
                    end_read = (
                        python_calls[list(aws_service_classes)[class_index + 1]][
                            "index"
                        ]
                        - 1
                    )
                else:
                    end_read = None

                # Read contents of AWS source rst, section where method exists
                # This is how the docstring content source is discovered
                with Path.open(
                    aws_rst_source_file, mode="r"
                ) as aws_rst_source_file_reader:
                    aws_rst_docstring = aws_rst_source_file_reader.read().split("\n")[
                        start_read:end_read
                    ]
                    # Begin: Processing and creating function docstring
                    #  Strip out trailing whitespace from rst,
                    #  don't include additional sections using ===* headers,
                    #  remove unnecessary blank line gaps larger than one
                    blank_lines = 0
                    for text in aws_rst_docstring:
                        text = text.rstrip()
                        if "===" in text:
                            break
                        elif text == "":
                            blank_lines += 1
                            if blank_lines == 2:
                                blank_lines = 1
                                continue
                            else:
                                idem_exec_file_contents.append(f"{text}\n")
                        else:
                            blank_lines = 0
                            idem_exec_file_contents.append(f"{text}\n")
                    # End: Processing and creating function docstring
                    idem_exec_file_contents.append('    """\n')

                    # Add post-docstring function code
                    idem_exec_file_contents.extend(
                        create_idem_awsauto_function_code(
                            aws_service_class,
                            aws_service_class_method["python_call"].replace(
                                ".. py:method:: ", ""
                            ),
                            aws_rst_docstring,
                        )
                    )
            # Write new .py file with generated functions and docstrings
            service_rst_file_idem = Path(
                py_file_dir, f"{py_file_name.split('(')[0]}.py"
            )
            with open(service_rst_file_idem, "w") as rst_writer:
                rst_writer.writelines(idem_exec_file_contents)
            class_index += 1
