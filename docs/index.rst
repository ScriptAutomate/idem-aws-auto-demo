.. idem-aws-auto documentation master file, created by
   sphinx-quickstart on Sun Oct 18 21:55:37 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

idem-aws-auto documentation
===========================

API reference
-------------

Services
~~~~~~~~

.. toctree::
   :maxdepth: 3
   :glob:

   ref/exec/**/index
