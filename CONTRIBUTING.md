# Contributing Guide

This section describes ways in which you can contribute if you have feedback for
improvements to the `idem-aws-auto` project.

## Open an issue

If you notice a problem, have a feature idea, or have general feedback, you can
open an issue in this project:

1. Click **Issues** in the side navigation, then select **List**.
2. Click the **New issue** button.
3. Fill out the necessary information to adequately describe the issue.

After opening an issue, it might help to reach out on the SaltStack Community slack to
for others to see the issue or contribute to the conversation.

## Overview of how to contribute to this repository

To contribute to this repository, you first need to set up a forked project:

- [Fork, clone, and branch the repo](#Fork-clone-and-branch-the-repo)
- [Set up your local preview environment](#set-up-your-local-preview-environment)

After this initial setup, you then need to:

- [Ensure your local repository is up to date with the master branch](#sync-local-master-branch-with-upstream-master)
- Edit the code and/or documentation
- [Preview your changes with a local Sphinx build](#preview-HTML-changes-locally)
- Open a merge request in Gitlab
- Ensure your merge request gets at least one approval

## Overview of the toolchain

This project is written in Python and reStructured Text (.rst), with docs statically
generated into HTML by Sphinx, presented in the SaltStack Material theme, and hosted on
Gitlab Pages.

- [reStructuredText Primer](https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html)
- [Sphinx](https://www.sphinx-doc.org/en/master/)
- [SaltStack Material Theme](https://gitlab.com/saltstack/open/docs/sphinx-material-saltstack)
- [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/) for hosting
- [GitLab CI](https://docs.gitlab.com/ee/ci/) for:
  - Running URL validation with [`brok`](https://github.com/smallhadroncollider/brok),
    while skipping links in `ignore-links.txt`.
  - Running `pre-commit` automated tests
  - Deploying the site to GitLab Pages

---

## Prerequisites
For local development, the following prerequisites are needed:

- [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
- [Python 3.6+](https://realpython.com/installing-python/)
- [Ability to create python venv](https://realpython.com/python-virtual-environments-a-primer/)

## Fork, clone, and branch the repo

SaltStack uses the fork and branch Git workflow. For an overview of this method,
see [Using the Fork-and-Branch Git Workflow](https://blog.scottlowe.org/2015/01/27/using-fork-branch-git-workflow/).

First, [create a new fork](https://gitlab.com/scriptautomate/idem-aws-auto/-/forks/new).
Fork the repository into your user namespace.

Then, clone the forked repo to your local machine:

```bash
# SSH
git clone git@gitlab.com:<forked-repo-path>/idem-aws-auto.git

# or HTTPS
git clone https://gitlab.com/<forked-repo-path>/idem-aws-auto.git
```

Configure the remotes for your main upstream repository:

```bash
# Move into cloned repo
cd idem-aws-auto

## Choose SSH or HTTPS upstream endpoint
# SSH
git remote add upstream git@gitlab.com:scriptautomate/idem-aws-auto.git
# or HTTPS
git remote add upstream https://gitlab.com/scriptautomate/idem-aws-auto.git
```

Create new branch for changes to submit:

```bash
git checkout -b <feature-branch-name>
```

## Set up your local preview environment

If you are not on a Linux machine, you need to set up a virtual environment to
preview your local changes and ensure the [dependencies](#prerequisites) are
available for a Python virtual environment.

From within your local copy of the forked repo:

```bash
# Setup venv
python3 -m venv .venv
# If Python 3.6+ is in path as 'python', use the following instead:
# python -m venv .venv

# Activate venv
source .venv/bin/activate

# Install required python packages to venv
pip install -U pip setuptools wheel
pip install -r requirements-docs.txt
pip install -r requirements.txt

# Setup pre-commit
pre-commit install
```

All required files should now be in place.


## What is pre-commit?

[**pre-commit**](https://pre-commit.com/) is a tool that will automatically run local tests when you attempt
to make a git commit. To view what tests are run, you can view the
`.pre-commit-config.yaml` file at the root of the repository.

One big benefit of pre-commit is that _auto-corrective measures_ can be done
to files that have been updated. This includes Python formatting best practices,
proper file line-endings (which can be a problem with repository contributors
using differing operating systems), and more.

If an error is found that cannot be automatically fixed, error output will help
point you to where an issue may exist. For example, `codespell` may find words
it thinks have been spelled incorrectly.

```
# Example output
Check for merge conflicts................................................Passed
Mixed line ending........................................................Passed
Fix End of Files.........................................................Passed
Check python ast.........................................................Passed
Check Yaml...............................................................Passed
Detect Private Key.......................................................Passed
black....................................................................Passed
codespell................................................................Passed
- hook id: codespell
- exit code: 1

README.md:232: wrong-word ==> suggested-word
```

In this example above, `codespell` flagged a word that it believes has been
spelled incorrectly:

```
README.md:232: wrong-word ==> suggested-word
```

Going back to the source and fixing the word will fix the problem, and the next
run will pass:

```
# Example output
Check for merge conflicts................................................Passed
Mixed line ending........................................................Passed
Fix End of Files.........................................................Passed
Check python ast.........................................................Passed
Check Yaml...............................................................Passed
Detect Private Key.......................................................Passed
black....................................................................Passed
codespell................................................................Passed
```

A GitLab CI job also runs pre-commit tests in the repository in case the
contributor has not configured it locally.

## Sync local master branch with upstream master

If needing to sync feature branch with changes from upstream master, do the
following:

> _NOTE: This will need to be done in case merge conflicts need to be resolved
> locally before a merge to master in the upstream repo._

```bash
git checkout master
git fetch upstream
git pull upstream master
git push origin master
git checkout <feature-branch-name>
git merge master
```

## Autogeneration workflow

The autogeneration is done by `./tools/generator.py`.

- Calls AWS API via `boto3` to generate `boto3` reStructuredText (`.rst`) files.
- Walks through source `.rst` files to create `.py` files in `./idem_aws_auto/exec/awsauto/*`
- Creates `.rst` files in the `./docs/ref/exec/*` directory, which use `.. automodule::` to reference the generated python

### Running the generator

```bash
# From SaltConf20 DemoJam
# venv
python3 -m venv .venv
source .venv/bin/activate
pip install -U pip setuptools wheel
pip install -r requirements.txt
pip install -r requirements-docs.txt

# Recursive file count before
ls -lR idem_aws_auto/exec/awsauto/ | wc -l

# If running the generator for the first time
## Generate source rst files, first, which is required
##  before running subsequent generator commands using the false arg
## Takes quite a bit longer, as this generates all
##  the source boto3 rST doc files from AWS API, so run once
##  during development, and use the false arg for subsequent runs
python tools/generator.py true ec2

# ELSE: If generator has previously been run, and the
## AWS rst source files exist, generator can skip the boto3 rst source
## download (which takes long)
## If args are left off, will generate ALL python files from source
## Can use any service name: ec2 apigateway amplify etc. (space-delimited list)
python tools/generator.py false ec2 apigateway amplify

# Recursive file count after
ls -lR idem_aws_auto/exec/awsauto/ | wc -l

# view all generated files in ec2/client.py
ls idem_aws_auto/exec/awsauto/ec2/

# ec2/client.py line count
cat idem_aws_auto/exec/awsauto/ec2/client.py | wc -l

# pydoc: review ec2.client.describe_instances function
# Emulating something close to potential pop-doc future feature?
# Similar to salt's sys.doc, but in pop: https://gitlab.com/saltstack/pop/pop/-/issues/92
python -m pydoc idem_aws_auto.exec.awsauto.ec2.client.describe_instances

# Generate Sphinx docs for new module
nox -e 'docs-html(compress=False, clean=True)'

# Sphinx site is viewable via ./docs/_build/html/index.html
## firefox ./docs/_build/html/index.html
## google-chrome-stable ./docs/_build/html/index.html
```
