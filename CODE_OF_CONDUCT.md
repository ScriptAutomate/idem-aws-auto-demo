# SaltStack Code of Conduct

Please refer to the [SaltStack Code of Conduct](https://github.com/saltstack/salt/blob/master/CODE_OF_CONDUCT.md), which is meant to be followed when it comes to all SaltStack projects.
