# Autogenerated AWS Cloud Provider for Idem

[![POP](https://img.shields.io/badge/made%20with-POP-blue)](https://pop.readthedocs.io/)
[![Idem](https://img.shields.io/badge/made%20with-Idem-blue)](https://idem.readthedocs.io/)
[![Slack](https://img.shields.io/badge/slack-@saltstackcommunity-blue.svg?logo=slack")](https://saltstackcommunity.slack.com)

***NOTE: This is a proof of concept created for demo purposes at SaltConf20.***

> _**idem-aws-auto** is an autogenerated set of `idem` plugins for AWS, with code and documentation built off of the `boto3` project by AWS._

## Administrator/User Path

Are you wanting to make use of `idem-aws-auto` for interacting with AWS?

### Prerequisites

- [Python 3.6+](https://realpython.com/installing-python/)

### Installation

`idem-aws-auto` can be installed and used to execute commands against AWS. It currently extends `idem-aws`, so installing it means it will be capable of doing everything `idem-aws` does along with the autogenerated functions created here.

#### Download from GitLab

```
# git / ssh
git clone git@gitlab.com:ScriptAutomate/idem-aws-auto.git

# Or git / https
git clone https://gitlab.com/ScriptAutomate/idem-aws-auto.git
```

## Contributing

Are you wanting to contribute to, or customize your own version of, `idem-aws-auto`?

See the [CONTRIBUTING.md]() file in this repository for more information.

## Contact

**Derek Ardolf** made this proof-of-concept and can be found as @ScriptAutomate on most platforms:

- [Twitter](https://twitter.com/ScriptAutomate)
- [SaltStack Community Slack](https://saltstackcommunity.slack.com)
- [GitHub](https://github.com/scriptAutomate/)
- [GitLab](https://gitlab.com/ScriptAutomate)

The best channels to talk about `idem-aws-auto` in the **SaltStack Community Slack**:

- `#idem-cloud`
- `#cloud`
- `#documentation`

## License

This projects uses the [Apache License 2.0 (Apache-2.0)]()
  - [TLDR breakdown of license](https://tldrlegal.com/license/apache-license-2.0-(apache-2.0))
